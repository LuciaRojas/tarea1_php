<?php
include_once 'ClConexion/ClConexion.php';


    if(isset($_POST['guardar'])){
    
        $nombre=$_POST['nombre'];
        $apellido=$_POST['apellido'];
        $num_telefono=$_POST['num_telefono'];
        $email=$_POST['email'];

        if(!empty($nombre) && !empty($apellido) && !empty($num_telefono) && !empty($email)){
              
				$consulta_insert=$con->prepare('INSERT INTO clientes(nombre,apellido,num_telefono,email) VALUES(:nombre,:apellido,:num_telefono,:email)');
				$consulta_insert->execute(array(
               
                    ':nombre' =>$nombre,
                    ':apellido' =>$apellido,
					':num_telefono' =>$num_telefono,
					':email' =>$email
				));
	
            echo "<script> alert('Se guardaron los datos');</script>";
        }else{

            echo "<script> alert('Los campos estan vacios');</script>";
        }

    }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="robots" content="noindex,nofollow">

    <title>Insertar Clientes</title>
</head>
<body>
    <div class="contenedor">
    <h2>Insertar Datos</h2>
    <form action="" method="post">
   
            <input type="text" name="nombre" placeholder="Nombre" class="input__text" required="" pattern="[a-zA-Z]+" > <br>
            <input type="text" name="apellido" placeholder="Apellido" class="input__text"  required="" pattern="[a-zA-Z]+" > <br>
        
            <input type="text" name="num_telefono" placeholder="número teléfono" class="input__text" required="" pattern="[0-9]+"> <br>
    
       
        <input type="text" name="email" placeholder="email" class="input__text" required=""> <br>
       
  
       

          <input type="submit" name="guardar" value="Guardar" class="btn__primary">
      
    </form>
    
    </div>
    
</body>
</html>